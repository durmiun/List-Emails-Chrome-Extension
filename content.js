InboxSDK.load('1.0', 'sdk_durmiun_edcf8f276b').then(function(sdk) {
	
	sdk.Toolbars.registerToolbarButtonForThreadView({
		title: 'Show Reversed Email List',
		iconUrl: chrome.extension.getURL('images/logo.png'),
		section: 'METADATA_STATE',
		onClick: function(event) {
				// create message object holding all message views of currently expanded messages
				// at the time the button is pressed
				var message = event.threadView.getMessageViews();

				// Create an array of messageViews (all expanded messages in the current thread)
				// and reverse the order of the emails so the latest is at the top
				var messages = [];
				message.reverse().forEach( function(messageView) {
					if(messageView.getViewState() != "COLLAPSED"){
						messages.push(messageView);
					}
				});

				// display all of the expanded messages in a modal window
				var el = '<div style="overflow-y: auto; overflow-x: auto; resize: both; height:80vh; width:80vw"><br>';
				messages.forEach( function(messageView) {
					el += '<strong><p>From:</strong> ' + messageView.getSender().name + ' (' + messageView.getSender().emailAddress + ')<br>';
					el += '<strong>Recipients (To, CC, and BCC):</strong> ';
					var recipients = [];				
					messageView.getRecipients().forEach(function(contacts) { recipients.push(contacts.name + ' (' + contacts.emailAddress + ')')});
					el += recipients.join(', ');
					el += '<br>';
					el += '<strong>Date:</strong> ' + messageView.getDateString() + '<br>';
					el += '<strong>Subject:</strong> ' + event.threadView.getSubject(); + '<br>';
					el += messageView.getBodyElement().innerHTML;
					el += '</p><hr>';
				});
				el += '</div>'
				
				sdk.Widgets.showModalView({el: el});
			}
	});

});